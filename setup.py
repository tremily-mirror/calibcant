# Copyright (C) 2008-2013 W. Trevor King <wking@tremily.us>
#
# This file is part of calibcant.
#
# calibcant is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# calibcant is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# calibcant.  If not, see <http://www.gnu.org/licenses/>.

"calibcant: tools for thermally calibrating AFM cantilevers"

from distutils.core import setup
from os import walk, listdir
import os.path

from calibcant import __version__


package_name = 'calibcant'

def find_packages(root=package_name):
    packages = []
    prefix = '.'+os.path.sep
    for dirpath,dirnames,filenames in walk(root):
        if '__init__.py' in filenames:
            if dirpath.startswith(prefix):
                dirpath = dirpath[len(prefix):]
            packages.append(dirpath.replace(os.path.sep, '.'))
    return packages

packages = find_packages()
scripts = [os.path.join('bin', f) for f in sorted(os.listdir('bin'))]

setup(name=package_name,
      version=__version__,
      maintainer='W. Trevor King',
      maintainer_email='wking@tremily.us',
      url='http://blog.tremily.us/posts/{}/'.format(package_name),
      download_url='http://git.tremily.us/?p={}.git;a=snapshot;h={};sf=tgz'.format(package_name, __version__),
      license='GNU General Public License v3 (GPLv3)',
      platforms=['all'],
      description=__doc__,
      long_description=open('README', 'r').read(),
      classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: POSIX',
        'Operating System :: Unix',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering',
        'Topic :: Software Development :: Libraries :: Python Modules',
        ],
      packages=packages,
      scripts=scripts,
      provides=['{} ({})'.format(package_name, __version__)],
      requires=['pypiezo (>= 0.7)'],
      )
